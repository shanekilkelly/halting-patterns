---
title: "The VSM as Diagram"
date: 2019-01-03T12:19:27Z
author: "Shane Kilkelly"
---

I've been reading a lot of (and about) Deleuze and Guattari over this last year.
Coming at this material as an outsider, someone who doesn't have a background in
academic theory, has been tough, but I feel like I've learned a lot in a
relatively short span of time. Absorbing these new ideas has had me analysing
everything I see in those terms. Deterritorialization, Recoding, Assemblage, these are all
are new toys to play with, and new lenses to peer through.

This year Santa brought me a very nice gift, a copy of Manuel DeLanda's
[Assemblage Theory](https://edinburghuniversitypress.com/book-assemblage-theory.html),
which has turned out to be an incredible read. I've been working to condense
all of my thoughts on this book, but in the meantime I've had a thought that
connects D&G to my other favourite thinker,
[Stafford Beer](https://en.wikipedia.org/wiki/Stafford_Beer).

In the introduction to Assemblage Theory, DeLanda mentions D&G's concept of
a Diagram:

> An ensemble in which components have been correctly matched together possess
> properties that its components do not have. It also has its own tendencies and
> capacities. The latter are real but not necessarily actual if they are not
> currently manifested or exercised. The term for something that is real but
> not actual is _virtual_. **An assemblage's Diagram captures this virtuality, the
> _structure of the possibility space_ associated with an assemblage's dispositions**.

(In fact, there's a whole chapter on Diagrams later in the book, which I haven't
gotten to yet, we'll see if this blog-post still holds up once I get there.)

So this is interesting, if a little tricky. I take this to mean that the Diagram
of a System (or Assemblage, Machine, Process or whatever you want to call it),
is the abstract set of things, relations, flows, operations, and so on that
direct the unfolding of that system. Tendencies and capacities. Almost like a
schema. The system might have a tendency to do X, even if it is not doing so at
the moment, and it might have the capacity to do Y.

The Diagram seems like an abstract structure that maps out
what a System can do. There is also an implication here that many concrete
Systems could share approximately the same Diagram, if their various flows and
dynamics were essentially the same in the abstract. (Again, as an amateur
student of D&G, it's possible that I'm misreading this, but whatever).

At the time I read this I couldn't really think of an example that made much
sense to me. Then I realized that Stafford Beer's "Viable System Model" (or VSM) is something like this concept of a Diagram.

![A simple representation of the VSM](/img/Simple_VSM.png)

Stafford Beer was a pioneer of Cybernetics, in particular the application of
cybernetics to human organisations and institutions. One of the major themes of
his work is a pre-occupation with the concept of Viability. A "Viable System" is
one that survives and thrives as it interacts with its environment. A non-viable
system crumbles as shocks arrive from the environment and the system is unable to
adapt. Beer posited that one of the major problems we face today is that most
(if not all) of our institutions and organisational forms are non-viable. They
don't adapt. They respond to shocks by simply reterritorializing and hardening
their boundaries, by clenching harder than before.

A Viable System, by contrast, is one that can constantly adapt to its changing
environment, and even its changing self. It is an autonomous system, made up of
recursive sub-systems which are themselves viable. It's a system which obeys
certain cybernetic principles, which embodies certain abstract tendencies and
capacities.

The human body is a Viable System. In fact, most complex biological systems are.
A Firm is (in it's ideal form) a Viable System, even if most firms in the world
today are not. The same goes for States, Unions, Clubs, Communities, any social
formation we can name, we can look at it through the lens of Beer's VSM and
discover something about the cybernetics of that formation. In his writing, Beer
makes explicit the point that the same abstract model of a system can map onto
many different concrete systems.

This is where the concept overlaps with Assemblage Theory, both are made up of
recursive collections of components where the whole is not reducible to its
parts, nor are the parts reducible to the whole. Assemblage Theory is much more
general, seeking to build a theory of assembled agency at multiple scales, and
without reference to the particular structure of each assemblage, but I find it
remarkable that so much of these contemporary concepts is already present in
Beer's writing on cybernetics.

In studying a given System/Assemblage (such as a Firm or a Community), we might
discover that a given system is already functioning in a way that
closely resembles the abstract information flows and dynamics of the VSM, or we
might find that this particular social assemblage is deficient in some way,
hobbling along without some essential function (such as System Three-Star in
Beer's terminology, the capacity for spontaneous self-audits). That capacity
would remain virtual, until someone in the organisation would point out the flaw
and make the change (or be ejected for defying the dominant code of the
organisation).

Or we might discover that the system is not Viable in the slightest. Its Diagram
is entirely different. It operates by an entirely different abstract logic, a
different schema. Perhaps its Diagram more closely resembles that belonging to a
wave on the ocean, heading to shore, cresting, and about to collapse.

----

I'm starting to think there might be potential in fusing Beer's Organisational
Cybernetics with the Deleuzian current of thought that has been developed over
the past few decades. The notion of "Viable Systems" can probably be expanded
greatly by re-framing in the context of Assemblage, and perhaps Beer's
Cybernetic Ontology can help save the Deleuzian current from the dour
anti-praxis that's so common in the Accelerationist sphere of thought.

-- S
