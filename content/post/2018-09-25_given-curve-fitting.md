---
title: "Given Curve-Fitting, What Is The Firm?"
date: 2018-09-25T16:41:09Z
author: "Shane Kilkelly"
---

Last week a good friend of mine was back in town for a few days, and we took the
opportunity to catch up over coffee. As usual the conversation turned rapidly to
technology and politics, current events, and so on.

This friend spent a couple of years working at Amazon, before moving on to a
Machine-Learning startup. We got to talking about our shared relative
disillusionment with the current state of Machine Learning, and supposed “AI”,
but, you know, the ability to do almost-arbitrary curve-fitting is still pretty
cool, right? That’s still useful even if it doesn’t lead to the creation of a
Synthetic Subject.

Anyway, my friend observed that in large companies there is a drive to
restructure the company and its systems to make data available to these
curve-fitting algorithms. Having “big data” is nowhere near enough, the whole
structure of the company changes to facilitate this technological shift. The
data must be available, it must be comprehensible, it must be possible to act on
the outputs from the algorithms.

This instantly reminded me of a passage in Stafford Beer’s “Brain of the Firm”,
a classic in Management Cybernetics. Back in the 60′s, Beer lamented that firms
were adopting computer technology and automating some processes, but the way
they were going about it was entirely wrong-headed. Instead of transforming the
firm to match the potentialities of the new technology, they were simply
transplanting their existing paper-based workflows into the computers, often
producing a resulting flow that was much more fragile than the old manual flow.

For Beer, the question should not be “how should we adopt computers?”, but
instead: “Given computers, what is the firm?”. My friend and I had even seen
first-hand (at a previous mutual employer) how this same problem repeats in the
21st century. It seems nothing has been learned in a half-century.

We’re faced with a similar question today, even with the limited form of Machine
Learning that is actually available. How can we avoid simply transplanting
existing structures and flows into the new context?

Given curve-fitting, what is the firm?

– S
