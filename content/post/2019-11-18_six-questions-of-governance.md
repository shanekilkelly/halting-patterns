---
title: "Self-Organisation: Six Questions of Governance"
date: 2019-11-18T19:59:31Z
draft: false
author: "Shane Kilkelly"
---

Imagine a group of people who have come together to work on some project. Maybe
four of five people, with one fairly concrete objective. Perhaps they are trying
to publish a magazine, journal, or newsletter of some kind. They know what they
want to acomplish, and they have some vague idea of how to go about doing it.
Let's pretend that these people are called Alice, Bob, Claire, Derek, and
Esther. Oh, and they all happen to be socialists.

We can think of this group of people as forming a kind of "Unit", which is
coupled to some kind of "Environment". This is a basic inside/outside
distinction, the content of the unit is distinct from everything else in the
world, but the unit is still coupled to the world.


![mag-2.png](/img/mag-2.png)


This unit will take in information about the state of the world, submissions
from prospective authors, advice from people who have done this kind of work
before, and many more things too numerous to mention. The unit will then output
into the world (hopefully) at least one issue of a magazine. Ideally the unit
will continue to publish new issues, becoming an ongoing process. The
unit will evaluate its own performance, keeping an eye on the state of the world
as it develops, listening carefully for feedback on each new issue. Maybe later
this unit will go on to produce a podcast, or host workshops, or any number of
other activities, but let's not get ahead of ourselves.

As the five people go about the activity of actually-producing-a-magazine, they
will find that it's not an easy task. Sometimes they have trouble organising
time for video calls to talk about important decisions to be made. Maybe they
find it difficult to balance their immediate tasks against longer-term planning.
They may even find that what little planning they do seems to never work out,
because the situation changes and shifts underneath their feet. 
Perhaps they even struggle to figure out what they need to be doing in any given
moment.

All of these problems suggest a solution, which is itself another problem: the
problem of Management.


![mag-3.png](/img/mag-3.png)


Now, as good socialists, Alice, Bob, Clair, Derek, and Esther are wary of this
term, this new thing glaring at them from the horizon as they stumble through
their days. They know they need to get serious about how they organise
themselves, but most of what they know about organising large projects comes
from the Capitalist workplace, with its "Managers", hierarchies, and power
structures. The dull alienation of work, the separation of the managers from
the managed, all give them pause. And yet, something has got to give because
they can't just go on floundering like this.

"Management", a dirty word. Let's discard the bad word and talk instead about
Governance.


## (Self) Governance and Cybernetics 

The word "Governance" comes from the Greek word "Kybernetes", which is also the
origin of the word "Cybernetics".

```text
Kybernetes → Cybernetes → Cybernetics
    ↘
      Kubernetes → Gubernator → Governor
                       ↘
                         Governance
```

"Kybernetes" means "Steersman". Cybernetics is all about adaptive navigation,
and the ways in which complex systems (such as living things, and groups of
living things, and societies of groups of living things) navigate their complex
environment.

Stafford Beer described Cybernetics as the science of effective organisation, of
effective *self-organisation*, which is exactly what this gang of Reds needs in
order to complete this project, and to become titans of the lefty publishing
world. It just so happens that we have an existing body of knowledge on this
science of effective organisation, and we'll be drawing on that body of
knowledge for the rest of this essay. In particular we'll draw from the work of
Stafford Beer and his Viable System Model (VSM).

Beer was convinced that it is possible to re-organise society toward autonomy
and liberation, preserving the freedoms of modernity while discarding the dead
weight of old tyrannies, and building new freedoms which we don't enjoy today. 

And I think he was right.


## Asking Questions

Now that we've repositioned ourselves onto more favourable - but less familiar -
terrain, we should stop to ask ourselves some very important questions about
Governance: the six "w"s, who? what? when? where? how? and why?


### Who?

*Who should do the Governing? Everyone.*

All five of our intrepid publishers should be involved in the process of
governing their collective activity.

Immediately we can break from the curse of "Management" as a personal (class)
role inhabited by particular persons, and instead make management a
de-personalized collective process which is immanent to the work process our
folks are involved in. In this framework, management is a technology to be
weilded by the workers in the interests of their society.


### What?

*What should be Governed? The primay activities of the unit.*

Remember that we started with a collection of
*people*, who came together to form a *unit*, which carries out some *process*.

We need to draw a distinction between the people and the process.

The lapdogs of Capital would of course tell us that it's the *people* who should
be managed (or governed, to use our terminology), but I strongly disagree.
People are capable of managing themselves, as they are self-organising units in their
own right. What needs to be managed is not the people, but *the process*. It's
the interactions between the people - the abstract web of activities they're
tangled up in - which needs to be governed. To try to govern the people would be to
replicate the paranoid tyranny of capitalist domination.

In the Viable System Model, each "Unit" is composed of smaller, autonomous
units, and so on recursively, and it's the *interactions* between these
autonomous sub-units which needs to be managed, not the units themselves. In
this structure each unit has the freedom to solve its own problems, plus a
cohesive support structure which can solve problems which the individual unit
cannot handle alone. Autonomy and Cohesion are not at odds with one another,
each is essential for the other to be possible.

Careful readers will note that it would have been possible to formulate the
`Who?` question above as "*Who* should be governed?", but that formulation would
miss the point of cybernetic governance entirely. That kind of crude domination is
characteristic of the governance paradigms we've inhereted from the Dark Ages,
and we must now admit that these paradigms don't actually work. At best they
provide ideological justification for brutality, and at worst they contribute to
the same instability that they claim to solve.

It's time to throw out those pre-scientific notions of organising and grow up as
a society.


### When?

*When should Governance be done? Continuously, as needed.*

Our group of people should do their governance activities regularly, at a
rate which is appropriate to the level of *need* for governance. Here we shift
the emphasis onto doing just-enough management and doing it at the right time,
in rhythm with the activities which are being managed.

The team should have regular check-ins (sometimes called "standups") to synchronise
themselves with each other and report any problems they've encountered. If
there's nothing to report then great! Less work to do!

They should hold regular (but less
frequent) meetings to determine what to do in the short term, and to sketch out
plans for the long term.

They should also keep in mind that the purpose of their project is not to sit
around in meetings all day, but to actually get something done. The governance
process must serve the primary activities of the unit, not the other way round.


### Where?

*Where should governance be done? Within the unit itself.*

![mag-4.png](/img/mag-4.png)


This answer resonates with our earlier answer about *who* whould do the
governing. The collective unit should govern itself, and be the site of that
governance. Once again we manage to detonate a traditional notion of management
as something transcendent, something which happens above and beyond the realm of
the managed. Our imaginary group should govern themselves, and do so within
their own workshop (even if that workshop is Skype).


### How?

*How should Governance be done? Democratically, scientifically, and dynamically.*

The process of self-governance should be caried out democratically, with the
appropriate people having an appropriate say in how their activities will be
regulated. The process should also be scientific, with the group coming to
understand their own governance needs through experimentation and analysis.

At all times the collective should remain dynamic, installing just enough
structure to assist themselves in steering. The collective organism should
always be able to adapt to its immediate situation, and retain the capacity to
adapt continuously as the situation changes.


### Why?

Why should this process be governed? Why bother thinking about regulation? Isn't
that just boring government-speak for legislation on the size of parking meters?

Because these people want to succeed and to not waste their own time. Because
without a framework for understanding their own activities they're at risk of
just falling apart and burning out, as so many have done before.

The classic example of a cybernetic device is the thermostat, something with
which we're all familiar. The thermostat regulates (or governs) the temperature
of a room, to ensure that the temperature is held stable. The temperature
can fluctuate for all kinds of reasons: weather effects from outside, people
entering and exiting the room, changes within the room itself, the list goes on.

But the thermostat does its best to counteract these instabilities. And it does
so by following a few simple rules and responding dynamically to its
environment. Without this clever governance device the temperature would
oscillate wildly. This example can be generalised to all kinds of complex
systems with many interacting parts, including social systems, such as a system
of people trying to produce a magazine. Or a system of people trying to enact
positive change in the world. Or a system of people who intend to take on and
defeat the most powerful emergent complex system our species has ever seen:
Capitalism.


## Taking the Problem Seriously

If we take our activity (our *activism*) seriously then we must also take the
problem of governance seriously. It's not easy to co-ordinate a bunch of people
doing a whole lot of things all at once. It's practically guaranteed that we'll
experience oscillation just like the thermostat does. So many socialists
are already familiar with this problem, and some have even given up hope that
the situation can ever improve. But we *do* have tools to help ourselves do
better, to help us self-organise *in a way which actually works* and which
doesn't rely on fairy dust and grit to magically get us through, somehow.

There's no point in being the beautiful losers who keep getting this wrong,
expecting things to spontaneously just work themselves out. Or even worse, being
the fools who still place our faith in out-dated organisational forms and modes
of authority which have proven to fail time and again. To be blunt for a moment:
it's time to stop fucking this up.

Our imaginary group of friends should take the problem seriously, then proceed
to taking the solution seriously, lest they shake themselves apart and burn out
as so many before them have done.

----

In future essays I will elaborate on the core concepts of Beerian Cybernetics
and how we can use them to organise ourselves effectively and to create a better
world for all.


## Further Reading

- [General Intellect Unit, Episode 038 - The Viable System
  Model](http://generalintellectunit.net/e/038-the-viable-system-model/)
- [The Viable System Model as a Framework for Understanding
  Organizations](https://www.researchgate.net/publication/265740055_The_Viable_System_Model_as_a_Framework_for_Understanding_Organizations)
