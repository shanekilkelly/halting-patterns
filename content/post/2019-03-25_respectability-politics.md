---
title: "Swarms, Respectability Politics, and Variety Engineering"
date: 2019-03-25T14:48:40Z
author: "Shane Kilkelly"
---

I've just started reading "The Uprising", by Franco Berardi, and while I
haven't made it very far in the text, I've already encountered an idea that
gave me pause. The author touches on Wiener's Cybernetics quite early —
which is in itself tantalizing — but follows quickly with an idea that
resonates with what we've read in Beer's "Designing Freedom".


## Swarms and Networks

In the early section titled "Swarm", the author lays down a challenge:

> If we want to understand something more about the present social
> subjectivity, the concept of the multitude needs to be complimented with
> the concepts of the network and swarm

Networks are pluralities of beings tied together by common procedures and
protocols, which enable interconnection and interoperation. Swarms are
pluralities whose behaviour are determined (to some extent) by their neural
programming.

We then get the suggestion that:

> In conditions of social hyper-complexity, human beings tend to act as a
> swarm

followed closely by:

> In a broader sense we may say that in the digital age, power is all about
> making things easy. In a hyper-complex environment that cannot be
> properly understood and governed by the individual mind, people will
> follow simplified pathways and will use complexity-reducing interfaces

This resonates strongly with Beer's ideas in Designing Freedom (and
throughout his whole career), and we specifically with the Law of Requisite
Variety. Humans and the social assemblages they make up must engage in some
kind of variety engineering to survive, most often variety reduction. This
imperative becomes even more pronounced in a hyper-complex environment,
where not only the individual human, but even the social assemblage, has
little chance of keeping up with the systemic complexity.


## Respectability Politics as Ineffective Variety Reduction

The problem with our contemporary moment is that systemic complexity is so
vast that it completely overwhelms both people and institutions, leaving us
with a feeling of being adrift on a churning sea. Our codings and
procedures worked, at one point, but now we find that the old ways are
insufficient for the complexity we're confronted with.

Unfortunately, in the face of this realisation, the standard response is
not to adjust the system so that complexity can be handled, but to simply
re-trench and double down on the old procedures and protocols. Needless to
say, clenching one's teeth and hardening one's shell is not going to stop
the current from tearing one to shreds.

At this point in the text, the notion of "Respectability Politics" popped into
my head. We can frame this set of ideas as an attempt to establish common
procedures that help deal with complexity, by reducing complexity. (I should be
clear here, I'm using respectability politics as a convenient umbrella term for
all the usual liberal procedures and mores, the rituals, codes, and protocols of
a hegemonic system that revels in it's own inertia. You might know this thing by
another name, but it's the same beast).

And this worked for a while, but the trouble is, as Beer recognized, that
the attenuator is installed in the wrong part of the loop. Systemic variety
continues to proliferate, while the _regulator_ (the institutions and
networks) has _its_ variety reduced. And any change which would shake up
the regulator and improve it's capacity to meet the systemic complexity is
rejected in favour of an impotent clenching reaction.

Contemporary liberalism has left itself hamstrung, perilously half-attached
to a wild, seething mass of untamed complexity, but the response to this
predicament is to doggedly insist on the sufficiency of the "way things
have always been done", and to insist, contrary to all evidence, that if
everyone would just use their indoor voice then we could go back to "the
way things were".


## A Way Out

So what's to be done with this? How can we break these circuits and avoid being
trapped in them in the future? A minimal commitment would be to ruthlessly
evaluate all of our codes and protocols, to evaluate everything in its fitness
for the task at hand. To do that, we need to pull complexity and cybernetic
principles into the core of our thinking. This is sure to drive the Centroids
nuts, as we will be fundamentally undermining the basis for their un-questioning
respect for "whatever happens to exist". To ask whether this or that institution
or practice actually manages to succeed in the task it sets itself precludes the
kind of dead-eyed capitulation that we're asked to perform in the name of respectability.

-- S
